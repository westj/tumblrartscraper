# Tumblr Art Scraper
#### An Experimental script to download your entire Tumblr art blog with images and metadata

***WARNING***: This script is intensive, will make your computer and Internet super slow whilst running.

**Another disclaimer**: This script doesn't actually "scrape" any blogs, it simply accesses the blog's entire post history, gets the URL of each image via the Tumblr API. The part that *might* be considered abuse of the platform is where it then downloads each and every single image at the same time.

## How to use:

First off, download and install [Node.js][nodejs] (preferably LTS). [npm][npm] should already be bundled with Node.js.

Then, once that's installed, clone or [download this Git's archive][git-archive].

Click `Register application` on [Tumblr's OAuth Applications page][tumblr-oauth].

- **Application Name**: Anything, e.g. "SockMonster's Backup App"
- **Application Website**: Anything, e.g. http://localhost/
- Ignore **App Store URL** and **Google Play Store URL**.
- **Application Description**: Anything, e.g. "Make a backup of my Tumblr"
- **Administrative contact email**: Your email address
- **Default callback URL**: Anything, e.g. http://localhost/
- Skip everything else, verify Captcha, and hit "Register".
- Under your application, click on "Show secret key" and copy this to a safe place. You'll need it later.

Now to get the "Scraper" working.

1. Open a terminal/command window:
    - Windows: Hit Windows + R, type `cmd`, then hit OK/enter.
    - mac OS: Open Terminal (under Applications -> Utilities).
    - Linux: Open your terminal emulator.
2. Type `cd ` (the space after is important), then drag the unzipped folder onto the window. Hit enter.
3. Run `npm install`.
4. When done, run `node . <YOUR TUMBLR LINK HERE> <OAUTH CONSUMER KEY>`
    Example: `node . sockmonster.tumblr.com wxen4ypJjtABaeeUCN5n3VP2LagohAn1M345l5yZbsOP7li2Y`
5. Wait for the program to finish.

**Note**: The `--prettify` flag will prettify JSON when saving.

This will output files in the following structure:

```
blog/
└ <USERNAME>
    ├ <OFFSET n>.json
    ├ ...
    └ photos
        ├ tumblr_<ID>_<HIGHEST RESOLUTION>.<EXTENSION>
	└ ...
```

Inside the `blog/<USERNAME>` folder you will see some `.json` files (blog metadata - descriptions, captions, note counts, etc.), and inside `blog/<USERNAME>/photos/` all the blog's images, downloaded at the highest resolution available.

If you have any issues, [create a new issue][new-issue].


[nodejs]: https://nodejs.org/
[npm]: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
[git-archive]: https://gitlab.com/westj/tumblrartscraper/-/archive/master/tumblrartscraper-master.zip
[tumblr-oauth]: http://www.tumblr.com/oauth/apps
[new-issue]: https://gitlab.com/westj/tumblrartscraper/issues/new

