const fs = require('fs');
const path = require('path');
const { URL } = require('url');
const util = require('util');

const axios = require('axios');

const pad = x => x < 10 ? '0' + x : x;

function log (...x) {
  const now = new Date();
  const date = now.getFullYear() + '-' + pad(now.getMonth() + 1) + '-' + pad(now.getDate());
  const time = pad(now.getHours()) + ':' + pad(now.getMinutes()) + ':' + pad(now.getSeconds());
  console.log(date, time, '-', ...x);
}

async function mkdir(path, exitOnFailure = true) {
  fs.promises.mkdir(path, { recursive: true }).catch(err => {
    if (err.code !== 'EEXIST') {
      log('Could not create folder', path, ':', err.message);
      if (exitOnFailure) {
        process.exit(1);
      }
    }
  });
}

async function downloadImage (url, imagePath) {
  try {
    log('Downloading image:', url)
    const response = await axios({ url, responseType: 'stream' });
    const stream = fs.createWriteStream(imagePath);
    response.data.pipe(stream);
    return [ true, '' ];
  } catch (err) {
    return [ false, err ];
  }
}

async function getImageURLs (imageURLs, blogURL, offset, apiKey, doPrettifyJSON) {
  try {
    const url = `https://api.tumblr.com/v2/blog/${blogURL}/posts/photo?type=photo&offset=${offset}&api_key=${apiKey}`;
    const response = await axios.get(url);
    const json = doPrettifyJSON ? JSON.stringify(response.data, null, 2) : JSON.stringify(response.data);
    await fs.promises.writeFile(path.join(__dirname, 'blog', blogURL, offset + '.json'), json + '\n');

    const { posts } = response.data.response;
    for (let i = 0; i < posts.length; i++) {
      // For each post
      const post = posts[i];
      if (typeof post.photos !== 'undefined') {
        for (let j = 0; j < post.photos.length; j++) {
          // For each photo
          const photo = post.photos[j];
          imageURLs.push(photo.original_size.url);
        }
      }
    }
    const photosPath = path.join(__dirname, 'blog', blogURL, 'photos');
    await mkdir(path.join(__dirname, 'blog', blogURL, 'photos'));
    imageURLs.forEach(async imageURL => {
      const url = new URL(imageURL);
      const fileName = path.basename(url.pathname);
      const imagePath = path.join(photosPath, fileName);
      const [ ok, err ] = await downloadImage(imageURL, imagePath);
      if (ok) {
        log('Downloaded image', imageURL);
      } else {
        log('Could not download image', imageURL, ':', err.message);
      }
    });
    return [ true, '', imageURLs ];
  } catch (err) {
    return [ false, err, [] ];
  }
}

async function main () {
  const lowerArgs = process.argv.map(arg => arg.toLowerCase());
  const nonFlagArgs = process.argv.filter(arg => !arg.startsWith('--'));

  const blogURLstr = nonFlagArgs[2];
  const apiKey = nonFlagArgs[3];
  const doPrettifyJSON = lowerArgs.includes('--prettify');

  if (typeof blogURLstr === 'undefined') {
    console.error('Missing blog URL')
    process.exit(1);
  } else if (typeof apiKey === 'undefined') {
    console.error('Missing API key')
    process.exit(1);
  }

  let blogURL;
  try {
    blogURL = new URL(blogURLstr);
  } catch (err) {
    if (err.code === 'ERR_INVALID_URL') {
      // Assume they just used the blog name.
      blogURL = new URL(`https://${blogURLstr}.tumblr.com`);
    }
  }
  let blogName = blogURL.hostname;
  const blogNameMatches = blogURL.hostname.match(/([\w-]+).tumblr/);
  if (blogNameMatches != null) {
    blogName = blogNameMatches[1];
  }
  await mkdir(path.join(__dirname, 'blog', blogName));
  
  const response = await axios.get(`https://api.tumblr.com/v2/blog/${blogName}/posts/photo?type=photo&api_key=${apiKey}`);
  const totalPosts = response.data.response.total_posts;
  log('Total posts to scrape:', totalPosts);
  
  let offset = 0;
  while (totalPosts > offset) {
    const [ ok, err, imageURLs ] = await getImageURLs([], blogName, offset, apiKey, doPrettifyJSON);
    if (!ok) {
      log('Could not get image URLs from', blogURLstr, ':', err.message);
    }
    offset += 20;
    if (offset + 19 > totalPosts) {
      log('Almost done...');
    }
  }
}
main();

process.on('exit', () => log('Done!'));

